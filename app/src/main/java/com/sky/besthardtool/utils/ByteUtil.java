package com.sky.besthardtool.utils;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by Administrator on 2017/12/12.
 */

public class ByteUtil {

    private static String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    /**
     * 字节数组转String
     *
     * @param results
     * @return
     */
    public static String byteArrayToString(byte[] results) {
        StringBuilder sb = new StringBuilder();
        for (byte result : results) {
            //per byte to string
            sb.append(byteToString(result));
        }
        return sb.toString();
    }

    /**
     * 字节转String
     *
     * @param b
     * @return
     */
    public static Object byteToString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hex[d1] + hex[d2];
    }

    /**
     * 字节数组拼接
     *
     * @param bt1
     * @param bt2
     * @return
     */
    public static byte[] byteMerger(byte[] bt1, byte[] bt2) {
        if (bt1 == null) {
            return bt2;
        } else {
            byte[] bt3 = new byte[bt1.length + bt2.length];
            System.arraycopy(bt1, 0, bt3, 0, bt1.length);
            System.arraycopy(bt2, 0, bt3, bt1.length, bt2.length);
            return bt3;
        }

    }

    /**
     * 16进制字符串转字节数组
     *
     * @param inHex
     * @return
     */
    static public byte[] HexToByteArr(String inHex)//hex字符串转字节数组
    {
        int hexlen = inHex.length();
        byte[] result;
        if (isOdd(hexlen) == 1) {//奇数
            hexlen++;
            result = new byte[(hexlen / 2)];
            inHex = "0" + inHex;
        } else {//偶数
            result = new byte[(hexlen / 2)];
        }
        int j = 0;
        for (int i = 0; i < hexlen; i += 2) {
            result[j] = HexToByte(inHex.substring(i, i + 2));
            j++;
        }
        return result;
    }

    /**
     * 判断奇数或偶数，位运算，最后一位是1则为奇数，为0是偶数
     *
     * @param num
     * @return
     */
    static public int isOdd(int num) {
        return num & 0x1;
    }

    /**
     * 16进制字符转整形
     *
     * @param inHex
     * @return
     */
    static public int HexToInt(String inHex)//Hex字符串转int
    {
        return Integer.parseInt(inHex, 16);
    }

    /**
     * 16进制字符转字节
     *
     * @param inHex
     * @return
     */
    static public byte HexToByte(String inHex)//Hex字符串转byte
    {
        return (byte) Integer.parseInt(inHex, 16);
    }

    /**
     * 计算字节数字异或值
     *
     * @param data
     * @param start 数组起始位置
     * @return 异或的结果
     */
    public static byte getXor(byte[] data,int start) {

        byte temp = data[start];

        for (int i = start+1; i < data.length; i++) {
            temp ^= data[i];
        }

        return temp;
    }

    public static String bytetostring(byte tByte){
        String tString = Integer.toBinaryString((tByte & 0xFF) + 0x100).substring(1);
        return tString;
    }

    public static int bytetoInt(byte b1,byte b2){
        char c = (char) (((b1 & 0xFF) << 8) | (b2 & 0xFF));
        return (int)c;
    }

    public static float bytetofloat(byte data1, byte data2, byte data3, byte data4) {
        byte[] mdata = {data1, data2, data3, data4};
        DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(mdata));
        float f = 0.0f;
        try {
            f = dataStream.readFloat();
            dataStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    public static  byte StringToByte(String bit) {
        int re, len;
        if (null == bit) {
            return 0;
        }
        len = bit.length();
        if (len != 4 && len != 8) {
            return 0;
        }
        if (len == 8) {// 8 bit处理
            if (bit.charAt(0) == '0') {// 正数
                re = Integer.parseInt(bit, 2);
            } else {// 负数
                re = Integer.parseInt(bit, 2) - 256;
            }
        } else {//4 bit处理
            re = Integer.parseInt(bit, 2);
        }

        return (byte) re;
    }


    public static String bytetoHex(byte be){
        switch (be){
            case 10:
                return "a";
            case 11:
                return "b";
            case 12:
                return "c";
            case 13:
                return "d";
            case 14:
                return "e";
            case 15:
                return "f";
            default:
                return be+"";
        }
    }

    public static byte[] charToByte(char c) {
        byte[] b = new byte[2];
        b[0] = (byte) ((c & 0xFF00) >> 8);
        b[1] = (byte) (c & 0xFF);
        return b;
    }

    public static byte[] IntToByteArray(int n) {
        byte[] b = new byte[4];
        b[0] = (byte) (n & 0xff);
        b[1] = (byte) (n >> 8 & 0xff);
        b[2] = (byte) (n >> 16 & 0xff);
        b[3] = (byte) (n >> 24 & 0xff);
        return b;
    }

    public static float getFloat(byte data1, byte data2, byte data3, byte data4) {
        byte[] mdata = {data1, data2, data3, data4};
        DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(mdata));
        float f = 0.0f;
        try {
            f = dataStream.readFloat();
            dataStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    static public String ByteArrToHex(byte[] inBytArr)//字节数组转转hex字符串
    {
        StringBuilder strBuilder=new StringBuilder();
        int j=inBytArr.length;
        for (int i = 0; i < j; i++)
        {
            strBuilder.append(Byte2Hex(inBytArr[i]));
            strBuilder.append(" ");
        }
        return strBuilder.toString();
    }

    static public String Byte2Hex(Byte inByte)//1字节转2个Hex字符
    {
        return String.format("%02x", inByte).toUpperCase();
    }


    public static byte getXor(byte[] datas){

        byte temp=datas[0];

        for (int i = 1; i <datas.length; i++) {
            temp ^=datas[i];
        }

        return temp;
    }


    public static byte[] hexStr2Bytes(String src){
        /*对输入值进行规范化整理*/
        src = src.trim().replace(" ", "").toUpperCase(Locale.US);
        //处理值初始化
        int m=0,n=0;
        int iLen=src.length()/2; //计算长度
        byte[] ret = new byte[iLen]; //分配存储空间

        for (int i = 0; i < iLen; i++){
            m=i*2+1;
            n=m+1;
            ret[i] = (byte)(Integer.decode("0x"+ src.substring(i*2, m) + src.substring(m,n)) & 0xFF);
        }
        return ret;
    }


}
