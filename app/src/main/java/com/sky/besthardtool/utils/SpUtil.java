package com.sky.besthardtool.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2017/12/26.
 */

public class SpUtil {

    static SharedPreferences sp;

    public static void putBaudrate(Context context, int bautrate) {


        //1、打开Preferences，名称为setting，如果存在则打开它，否则创建新的Preferences
        sp = context.getSharedPreferences("connectInfo", 0);

        //2、让setting处于编辑状态
        SharedPreferences.Editor editor = sp.edit();

        //3、存放数据
        editor.putInt("baudRate", bautrate);

        //4、完成提交
        editor.commit();
    }


    public static int getBaudrate(Context context) {

        //1、获取Preferences
        sp = context.getSharedPreferences("connectInfo", 0);

        //2、取出数据
        int bValue = sp.getInt("baudRate", 115200);
        return bValue;
    }

    public static void putCom(Context context, String com) {


        //1、打开Preferences，名称为setting，如果存在则打开它，否则创建新的Preferences
        sp = context.getSharedPreferences("connectInfo", 0);

        //2、让setting处于编辑状态
        SharedPreferences.Editor editor = sp.edit();

        //3、存放数据
        editor.putString("com", com);

        //4、完成提交
        editor.commit();
    }


    public static String getCom(Context context) {

        //1、获取Preferences
        sp = context.getSharedPreferences("connectInfo", 0);

        //2、取出数据
        String com = sp.getString("com", "/dev/ttyS3");
        return com;
    }
}
