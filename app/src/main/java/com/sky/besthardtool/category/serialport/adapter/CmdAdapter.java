package com.sky.besthardtool.category.serialport.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sky.besthardtool.R;


/**
 * Created by Administrator on 2017/12/21.
 */

public class CmdAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final String[] cmd;

    public CmdAdapter(Context context, String[] cmd) {
        this.inflater = LayoutInflater.from(context);
        this.cmd = cmd;
    }

    @Override
    public int getCount() {
        return cmd.length;
    }

    @Override
    public Object getItem(int position) {
        return cmd[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null){
            view = inflater.inflate(R.layout.item_serial_spinner,parent,false);
            holder = new Holder();
            holder.tvIns = view.findViewById(R.id.tv_ins);
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }

        holder.tvIns.setText(cmd[position]);

        return view;
    }


    private static class Holder{
        TextView tvIns;
    }
}
