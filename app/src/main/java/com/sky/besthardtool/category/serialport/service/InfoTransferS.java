package com.sky.besthardtool.category.serialport.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;


import com.sky.besthardtool.category.serialport.manager.TransferManager;
import com.sky.besthardtool.utils.ByteUtil;

import java.io.IOException;

/**
 * Created by Administrator on 2017/12/27.
 */

public class InfoTransferS extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        //只执行一次
        TransferManager.getInstance().connect(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("===============OnStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TransferManager.getInstance().disConnect();
//        System.out.println("===============OnDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new InfoTransferBinder();
    }


    public class InfoTransferBinder extends Binder {

        public InfoTransferS getService() {
            return InfoTransferS.this;
        }

        public void sendCommand(byte[] command) throws IOException {
            System.out.println("======>发送数据:"+ ByteUtil.byteArrayToString(command));
            TransferManager.getInstance().outputStream.write(command);
        }
    }
}
