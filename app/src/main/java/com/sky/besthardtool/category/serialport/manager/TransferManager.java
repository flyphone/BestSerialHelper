package com.sky.besthardtool.category.serialport.manager;

import android.content.Context;


import com.sky.besthardtool.category.serialport.api.SerialPort;
import com.sky.besthardtool.category.serialport.listener.OnIOTransferListener;
import com.sky.besthardtool.utils.SpUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by Administrator on 2017/12/27.
 */

public class TransferManager {

    private static TransferManager instance;
    private SerialPort serialPort;
    private FileInputStream inputStream;
    public FileOutputStream outputStream;
    private ReadThread mReadThread;
    private OnIOTransferListener listener;

    private TransferManager() {
    }

    public static TransferManager getInstance() {
        if (instance == null) {
            synchronized (TransferManager.class) {
                if (instance == null) {
                    instance = new TransferManager();
                }
            }
        }
        return instance;
    }

    public void connect(Context context) {
        String com = SpUtil.getCom(context);
        int baudRate = SpUtil.getBaudrate(context);
        try {
            serialPort = new SerialPort(com, baudRate);
            inputStream = (FileInputStream) serialPort.getInputStream();
            outputStream = (FileOutputStream) serialPort.getOutputStream();

            mReadThread = new ReadThread();
            mReadThread.start();
        } catch (Exception e) {
            listener.onSerialConnect(false);
            e.printStackTrace();
        }
    }

    public void disConnect() {
        if (mReadThread != null) {
            mReadThread.interrupt();
            mReadThread = null;
        }
        try {
            if (inputStream != null) {
                inputStream.close();
                inputStream = null;
            }
            if (outputStream != null) {
                outputStream.close();
                outputStream = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (serialPort != null) {
            serialPort.close();
        }
    }

    private class ReadThread extends Thread {
        public void run() {
            int nMaxBufLength = 64;
            byte[] buffer = new byte[nMaxBufLength];
            while (!isInterrupted()) {
                try {
                    int byteRead;
                    Thread.sleep(50);

                    if (inputStream != null) {

                        byteRead = inputStream.read(buffer);
                        if (byteRead >= 0) {
                            if (listener != null) {
                                byte[] sendBytes = new byte[byteRead];
                                System.arraycopy(buffer, 0, sendBytes, 0, byteRead);
                                listener.onReceive(sendBytes, byteRead);
                            }
                        }
                    } else {
//                        P("mInputStream==null");
                        break;
                    }
                } catch (IOException e) {
//                    P("IOException");
                    if (listener != null) {
                        listener.onSerialConnect(false);
                    }
                    break;
                } catch (InterruptedException e) {
//                    P("InterruptedException");
                    e.printStackTrace();
                }
            }//while(!isInterrupted())
        }
    }

    public void setOnIOTransferListener(OnIOTransferListener listener) {
        this.listener = listener;
    }


}
