package com.sky.besthardtool.category.serialport.listener;

/**
 * Created by Administrator on 2017/12/27.
 */

public interface OnIOTransferListener {
    void onReceive(byte[] data, int size);
    void onSerialConnect(boolean isConnected);
}
